package ClassiAstratte;

import Interface.Color;
import Interface.Shape;

                    //abstractFactory
public abstract class AbstractFactory {
	
	public abstract Color getColor(String color);
	public abstract Shape getShape(String shape);
}
