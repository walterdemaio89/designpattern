package Interface;

public interface Figure {
	
	public double calcolaArea();
	public double calcolaPerimetro();
	public void setBase(double base);
	public void setAltezza(double altezza);
	

}
