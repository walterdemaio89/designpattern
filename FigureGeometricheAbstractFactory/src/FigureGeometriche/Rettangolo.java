package FigureGeometriche;

import Colors.Red;
import Interface.Figure;
import Interface.Shape;

public class Rettangolo implements Figure, Shape {
	
	private double base;
	private double altezza;
	
	public Rettangolo() {
		
	}

	public void setBase(double base) {
		this.base = base;
	}

	public void setAltezza(double altezza) {
		this.altezza = altezza;
	}

	@Override
	public double calcolaArea() {
		double area = base * altezza;
		return area;
	}

	@Override
	public double calcolaPerimetro() {
		double perimetro = (base + altezza) * 2;
		return perimetro;
	}

	@Override
	public void draw() {
		System.out.println("Il rettangolo � di colore: " + new Red());		
	}
	
	

}
