package Svolgimento;

import java.util.Scanner;

import ClassiAstratte.AbstractFactory;
import FigureGeometriche.Rettangolo;
import Interface.Color;
import Interface.Figure;
import Interface.Shape;

             //client
public class EseguoCalcoli {

	public static void main(String[] args) {
		
		System.out.println("Su quale figura geometrica vuoi svolgere i tuoi calcoli?");

		System.out.println("1==RETTANGOLO");
		System.out.println("2==QUADRATO");

		Scanner input = new Scanner(System.in);

		int scelta = input.nextInt();

		AbstractFactory shapeFactory = FactoryProducer.getFactory("SHAPE");
		AbstractFactory colorFactory = FactoryProducer.getFactory("COLOR");

		if(scelta==1){
			System.out.println("Calcoliamo l'area di un rettangolo");
			Shape shape1 = shapeFactory.getShape("QUADRATO");
			Color color1 = colorFactory.getColor("RED");
			Figure f = new Rettangolo();
			//Shape shape2 = shapeFactory.getShape("RETTANGOLO");
			boolean flag=true;
			while(flag){
				System.out.println("inserire la base del rettangolo");
				double base=input.nextDouble();
				f.setBase(base);

				System.out.println("Inserire l'altezza del rettangolo");
				double altezza=input.nextDouble();
				f.setAltezza(altezza);

				if(base==altezza){
					flag=true;
					System.out.println("Devi inserire valori diversi! Voglio un rettangolo che non sia anche un quadrato!");
				}else{
					flag=false;
					System.out.println("L'area del rettangolo �: " + f.calcolaArea());
					System.out.println("Il perimetro del rettangolo �: " + f.calcolaPerimetro());
					shape1.draw();
					//color1.fill();
				}
			}

		}

		/*if(scelta==2) {
			System.out.println("Calcoliamo l'area di un quadrato");
			Quadrato q = fac.getQuadrato("Quadrato");
			System.out.println("Inserire il lato");
			q.setLato(input.nextDouble());

			System.out.println("L'area del quarato �: " + q.calcolaArea());
			System.out.println("Il perimetro del quadrato �: " + q.calcolaPerimetro());		
		}*/

		input.close();

		
		

	}

}
