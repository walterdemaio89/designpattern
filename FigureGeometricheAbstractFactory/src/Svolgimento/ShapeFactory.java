package Svolgimento;

import ClassiAstratte.AbstractFactory;
import FigureGeometriche.Quadrato;
import FigureGeometriche.Rettangolo;
import Interface.Shape;
import Interface.Color;

                                 //ConcreteFacory
public class ShapeFactory extends AbstractFactory {

	@Override
	public Color getColor(String color) {
		return null;
	}

	@Override
	public Shape getShape(String shapeType) {
		if(shapeType == null) {
			return null;
		}

		if(shapeType.equalsIgnoreCase("Quadrato")) {
			return new Quadrato();
		}

		
		else if(shapeType.equalsIgnoreCase("Rettangolo")) {
			return new Rettangolo();
		}
		
		return null;
	}

}
