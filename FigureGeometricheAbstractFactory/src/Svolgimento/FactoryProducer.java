package Svolgimento;

import ClassiAstratte.AbstractFactory;

public class FactoryProducer {
	public static AbstractFactory getFactory(String scelta) {
		if(scelta.equalsIgnoreCase("COLOR")) {
			return new ColorFactory();
		}

		else if(scelta.equalsIgnoreCase("SHAPE")) {
			return new ShapeFactory();
		}

		return null;

	}

}
