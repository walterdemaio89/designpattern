package Svolgimento;

import ClassiAstratte.AbstractFactory;
import Colors.Blue;
import Colors.Red;
import Interface.Color;
import Interface.Shape;

                                 //ConcreteFacory
public class ColorFactory extends AbstractFactory {

	@Override
	public Color getColor(String color) {
		if(color == null) {
			return null;
		}

		if(color.equalsIgnoreCase("Blue")) {
			return new Blue();
		}

		else if(color.equalsIgnoreCase("Rosso")) {
			return new Red();
		}

		return null;
	}

	@Override
	public Shape getShape(String shapeType) {
		return null;
	}

}
