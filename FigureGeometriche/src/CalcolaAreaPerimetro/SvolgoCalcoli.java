package CalcolaAreaPerimetro;

import java.util.Scanner;

import ImplementaMetodi.Quad;
import ImplementaMetodi.Rett;
import ImplementaMetodi.Triang;

public class SvolgoCalcoli {

	public static void main(String[] args) {

		System.out.println("Su quale figura geometrica vuoi svolgere i tuoi calcoli?");

		System.out.println("1==RETTANGOLO");
		System.out.println("2==TRIANGOLO");
		System.out.println("3==QUADRATO");

		Scanner input = new Scanner(System.in);

		int scelta = input.nextInt();

		Rett rettangoli = new Rett();
		Triang triangoli = new Triang();
		Quad quadrati = new Quad();

		if(scelta==1){

			System.out.println("Calcoliamo l'area di un rettangolo");
			boolean flag=true;
			while(flag){
				System.out.println("inserire la base del rettangolo");
				double base=input.nextDouble();
				rettangoli.setBase(base);

				System.out.println("Inserire l'altezza del rettangolo");
				double altezza=input.nextDouble();
				rettangoli.setAltezza(altezza);

				if(base==altezza){
					flag=true;
					System.out.println("Devi inserire valori diversi! Voglio un rettangolo che non sia anche un quadrato!");
				}else{
					flag=false;

					System.out.println("L'area del rettangolo �: " + rettangoli.calcolaArea());
					System.out.println("Il perimetro del rettangolo �: " + rettangoli.calcolaPerimetro());
				}
			}

		}

		if(scelta==2) {
			System.out.println("Calcoliamo l'area di un triangolo");

			System.out.println("Inserire la base del triangolo: ");
			triangoli.setBase(input.nextDouble());

			System.out.println("Inserire l'altezza del triangolo");
			triangoli.setAltezza(input.nextDouble());

			System.out.println("Inserire il primo lato");
			triangoli.setL1(input.nextDouble());

			System.out.println("Inserire il secondo lato");
			triangoli.setL2(input.nextDouble());

			System.out.println("L'area del triangolo �: " + triangoli.calcolaArea());
			System.out.println("Il perimetro del triangolo �: " + triangoli.calcolaPerimetro());

		}

		if(scelta==3) {

			System.out.println("Calcoliamo l'area di un quadrato");
			System.out.println("Inserire il lato");
			quadrati.setLato(input.nextDouble());

			System.out.println("L'area del quarato �: " + quadrati.calcolaArea());
			System.out.println("Il perimetro del quadrato �: " + quadrati.calcolaPerimetro());		

		}

		input.close();


	}

}


