package ImplementaMetodi;

import Interface.Rettangolo;

public class Rett implements Rettangolo{
	
	private double base;
	
	private double altezza;
	
	public Rett() {
		
		
	}
	
	/*public Rett(double base, double altezza) {
		
		this.base = base;
		this.altezza = altezza;		
	}*/
	
	
	/*public double getBase() {
		return base;
	}*/


	public void setBase(double base) {
		this.base = base;
	}

	/*public double getAltezza() {
		return altezza;
	}*/

	public void setAltezza(double altezza) {
		this.altezza = altezza;
	}

	@Override
	public double calcolaArea() {
		double area = base * altezza;
		return area;
	}

	@Override
	public double calcolaPerimetro() {
		double perimetro = (base + altezza) * 2;
		return perimetro;
	}
	
	

}
