package ImplementaMetodi;

import Interface.Quadrato;

public class Quad implements Quadrato {
	
	private double lato;
	
	public Quad() {
		
		
	}
	
	/*public Quad(double lato) {
		this.lato = lato;
	}*/
	
	
	/*public double getLato() {
		return lato;
	}*/

	public void setLato(double lato) {
		this.lato = lato;
	}

	@Override
	public double calcolaArea() {
		double area = lato * lato;
		return area;
	}

	@Override
	public double calcolaPerimetro() {
		double perimetro = lato * 4;
		return perimetro;
	}

}
