package ImplementaMetodi;

import Interface.Triangolo;

public class Triang implements Triangolo {

	private double base;
	private double altezza;	
	private double l1;
	private double l2;

	public Triang() {

	}

	/*public Triang(double base, double altezza, double l1, double l2) {

		this.base = base;
		this.altezza = altezza;
		this.l1 = l1;
		this.l2 = l2;		
	}*/

	/*public double getBase() {
		return base;
	}*/

	public void setBase(double base) {
		this.base = base;
	}

	/*public double getAltezza() {
		return altezza;
	}*/

	public void setAltezza(double altezza) {
		this.altezza = altezza;
	}

	/*public double getL1() {
		return l1;
	}*/

	public void setL1(double l1) {
		this.l1 = l1;
	}


	/*public double getL2() {
		return l2;
	}*/

	public void setL2(double l2) {
		this.l2 = l2;
	}

	@Override
	public double calcolaArea() {
		double area = (base * altezza) / 2;
		return area;
	}

	@Override
	public double calcolaPerimetro() {
		double perimetro = base + l1 + l2;
		return perimetro;
	}

}
