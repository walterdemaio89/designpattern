package Interface;

public interface Triangolo {
	
	public double calcolaArea();
	
	public double calcolaPerimetro();

}
