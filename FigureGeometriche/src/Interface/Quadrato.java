package Interface;

public interface Quadrato {

	public double calcolaArea();

	public double calcolaPerimetro();

}
