package Interface;

public interface Rettangolo {

	public double calcolaArea();
	
	public double calcolaPerimetro();
	
}
